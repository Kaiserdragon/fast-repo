repo_url = "https://rfc2822.gitlab.io/Kaiserdragon/fast-repo/"
repo_name = "FastRepo (unofficial)"
repo_icon = "fdroid-icon.png"
repo_description = """
Fast apk release
"""

archive_older = 0

local_copy_dir = "/fdroid"

keystore = "../keystore.bks"
repo_keyalias = "repokey"
